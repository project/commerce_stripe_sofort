<?php

namespace Drupal\commerce_stripe_sofort\Controller;

use Drupal;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\commerce_stripe_sofort\Event\CommerceStripeSofortEvent;
use Drupal\commerce_stripe_sofort\Event\CommerceStripeSofortEvents;
use Stripe\Charge;
use Stripe\Exception\ApiErrorException;
use Stripe\Exception\InvalidRequestException;
use Stripe\Stripe;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class StripeWebhookController.
 */
class StripeWebhookController extends ControllerBase {

  public function __construct() {
    $stripeSofortConfiguration = Drupal::config('commerce_payment.commerce_payment_gateway.commerce_sofort_stripe');

    if (!is_null($secret = $stripeSofortConfiguration->get('configuration.secret_key'))) {
      Stripe::setApiKey($secret);
    }
  }

  /**
   * Capture the payload.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   A simple string and 200 response.
   */
  public function capture(Request $request) {
    $response = new Response();
    $payload = $request->getContent();
    if (empty($payload)) {
      $message = 'The payload was empty.';
      Drupal::logger('commerce_stripe_sofort')->error($message);
      $response->setContent($message);
      return $response;
    }

    // Add the $payload to our defined queue.
    $updateOrder = $this->updateOrder(Json::decode($payload));
    if ($updateOrder instanceof Payment) {
      $response->setContent('Order updated');
    }
    else {
      $response->setContent('Order not updated');
    }
    return $response;
  }

  /**
   * Simple authorization using a token.
   *
   * @param string $token
   *    A random token only your webhook knows about.
   *
   * @return AccessResult
   *   AccessResult allowed or forbidden.
   */
  public function authorize() {
    return AccessResult::allowed();
  }

  /**
   * Check the Stripe Charge if the order is paid and update the order status
   * accordingly
   *
   * @param array $data
   *
   * @return void
   */
  private function updateOrder($data) {
    try {
      $chargeId = $data['data']['object']['id'];
      $charge = Charge::retrieve($chargeId);
      //update the order it is true
      $order = Drupal::entityTypeManager()
        ->getStorage('commerce_order')
        ->load($charge->source->metadata->order_id);

      if (!is_null($order)) {
        $sourceId = $order->getData('commerce_stripe_sofort_source');
        $sourceSecret = $order->getData('commerce_stripe_sofort_secret');
        $payment = $this->findPayment($sourceId, $sourceSecret);

        if ($payment instanceof Payment) {
          if (!$order->isPaid() && $charge->paid == TRUE) {
            $workflow = $payment->getState()->getWorkflow();
            $transition = $workflow->getTransition('capture');
            $payment->getState()->applyTransition($transition);
            $payment->setCompletedTime(Drupal::time()->getRequestTime());
            $payment->save();
            $event = new CommerceStripeSofortEvent($payment);
            Drupal::entityTypeManager()->getStorage('commerce_log')
              ->generate($order, 'commerce_stripe_sofort.capture_success', [
                'remote_id' => $payment->getRemoteId(),
              ])->save();
            Drupal::service('event_dispatcher')
              ->dispatch(CommerceStripeSofortEvents::COMMERCE_STRIPE_SOFORT_PAYMENT_SUCCEEDED, $event);
          }
          elseif ($charge->status == Charge::STATUS_FAILED) {
            $workflow = $payment->getState()->getWorkflow();
            $transition = $workflow->getTransition('void');
            $payment->getState()->applyTransition($transition);
            $payment->save();
            $event = new CommerceStripeSofortEvent($payment);
            Drupal::service('event_dispatcher')
              ->dispatch(CommerceStripeSofortEvents::COMMERCE_STRIPE_SOFORT_PAYMENT_FAILED, $event);

            Drupal::entityTypeManager()->getStorage('commerce_log')
              ->generate($order, 'commerce_stripe_sofort.capture_declined', [
                'remote_id' => $payment->getRemoteId(),
              ])->save();
          }
        }
        return $payment;
      }
    } catch (ApiErrorException  $e) {
      throw new InvalidRequestException('Unable to fetch Stripe payment source');
      Drupal::logger('commerce_stripe_sofort')->notice($e->getMessage());
    }
  }

  /**
   * public function findPayment
   * Check if a payment exists by remote_id (stripe source id) and return it
   *
   * @param array $data
   *
   * @return $payment instanceof Payment or null
   */
  public function findPayment($remote_id, $client_secret = FALSE) {
    $payments = Drupal::entityTypeManager()
      ->getStorage('commerce_payment')
      ->loadByProperties([
        'remote_id' => $remote_id,
      ]);

    if (empty($payments)) {
      return NULL;
    }

    $payment = reset($payments);
    if ($payment instanceof Payment && $client_secret) {
      $order = $payment->getOrder();
      if ($order instanceof Order && $client_secret != $order->getData('commerce_stripe_sofort_secret')) {
        $payment = NULL;
      }
    }
    return $payment;
  }

}
