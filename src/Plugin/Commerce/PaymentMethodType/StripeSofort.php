<?php

namespace Drupal\commerce_stripe_sofort\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Provides the Sofort payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "commerce_stripe_sofort",
 *   label = @Translation("Stripe Sofort"),
 *   create_label = @Translation("Stripe Sofort"),
 * )
 */
class StripeSofort extends PaymentMethodTypeBase {

    /**
     * {@inheritdoc}
     */
    public function buildLabel(PaymentMethodInterface $payment_method) {
        return $this->pluginDefinition['label'];
    }
}
