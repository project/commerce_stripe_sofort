<?php

namespace Drupal\commerce_stripe_sofort\Plugin\Commerce\PaymentGateway;

use Drupal;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\Payment;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsVoidsInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Uuid\Php;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\commerce_stripe_sofort\Controller\StripeWebhookController;
use Drupal\commerce_stripe_sofort\Event\CommerceStripeSofortEvent;
use Drupal\commerce_stripe_sofort\Event\CommerceStripeSofortEvents;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Stripe\Balance;
use Stripe\Charge;
use Stripe\Exception\ApiErrorException;
use Stripe\Source;
use Stripe\Stripe;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "commerce_stripe_sofort",
 *   label = "Sofort (Stripe)",
 *   payment_type = "payment_default",
 *   display_label = "Sofort",
 *   forms = {
 *     "offsite-payment" =
 *   "Drupal\commerce_stripe_sofort\PluginForm\StripeSofort\PaymentOffsiteForm",
 *   },
 * )
 */
class StripeSofort extends OffsitePaymentGatewayBase implements SupportsRefundsInterface, SupportsVoidsInterface {

  /**
   * Stripe webhook
   *
   * @var \DDrupal\commerce_stripe_sofort\Controller\StripeWebhookController
   */
  protected $stripeWebhook;

  /**
   * Payment Storage.
   *
   * @var \Drupal\commerce_payment\PaymentStorage
   */
  protected $paymentStorage;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * UUID core service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, LoggerInterface $logger, UuidInterface $uuid, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    if ($this->configuration['secret_key']) {
      Stripe::setApiKey($this->configuration['secret_key']);
      $this->logger = $logger;
    }
    try {
      $this->paymentStorage = $this->entityTypeManager->getStorage('commerce_payment');
    } catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $this->logger->critical($e->getMessage());
    }
    $this->uuid = $uuid;
    $this->eventDispatcher = $event_dispatcher;
    $this->stripeWebhook = new StripeWebhookController();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('logger.factory')->get('commerce_stripe_sofort'),
      $container->get('uuid'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'publishable_key' => '',
        'secret_key' => '',
        'signing_secret' => '',
        'logo' => 0,
        'display_title_override' => '',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['publishable_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Publishable Key'),
      '#default_value' => $this->configuration['publishable_key'],
      '#required' => TRUE,
    ];


    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Key'),
      '#default_value' => $this->configuration['secret_key'],
      '#required' => TRUE,
    ];


    $form['signing_secret'] = [
      '#type' => 'textfield',
      '#title' => t('Signing secret'),
      '#description' => t('Endpoint for creating a Stripe signature is "' . Drupal::request()
          ->getSchemeAndHttpHost() . '/stripe-sofort-webhook". Sofort is an async payment gateway, so all webhooks must be enabled for all CHARGE events.<br>Check https://stripe.com/docs/webhooks/signatures for more info'),
      '#default_value' => $this->configuration['signing_secret'],
      '#required' => TRUE,
    ];
    $form['logo'] = [
      '#type' => 'checkbox',
      '#title' => t('Show logo only'),
      '#default_value' => $this->configuration['logo'],
    ];

    $form['display_title_override'] = [
      '#type' => 'textfield',
      '#title' => t('Override display title'),
      '#description' => t('Used for overriding display title. You may use HTML here if you want'),
      '#default_value' => $this->configuration['display_title_override'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      // Validate the secret key.
      $expected_livemode = $values['mode'] == 'live';
      if (!empty($values['secret_key'])) {
        try {
          Stripe::setApiKey($values['secret_key']);
          // Make sure we use the right mode for the secret keys.
          if (
            Balance::retrieve()
              ->offsetGet('livemode') != $expected_livemode
          ) {
            $form_state->setError($form['secret_key'], $this->t('The provided secret key is not for the selected mode (@mode).', ['@mode' => $values['mode']]));
          }
        } catch (ApiErrorException $e) {
          $form_state->setError($form['secret_key'], $this->t('Invalid secret key.'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      #hotfix if signing_secret is not assigned

      $this->configuration['secret_key'] = $values['secret_key'];
      $this->configuration['publishable_key'] = $values['publishable_key'];
      $this->configuration['signing_secret'] = $values['signing_secret'];
      $this->configuration['logo'] = $values['logo'];
      $this->configuration['display_title_override'] = $values['display_title_override'];
    }
  }

  /**
   * Create Authorisation Request.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   Payment.
   * @param string $returnUrl
   *   Return url.
   *
   * @return string
   *   Redirect Url.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createRequest(PaymentInterface $payment, $returnUrl) {
    if ($payment->getState()->value != 'new') {
      throw new InvalidArgumentException('The provided payment is in an invalid state.');
    }

    $url = FALSE;
    $payment_amount = $payment->getAmount();

    // Stripe Sofort payments require euro's. We can't convert here because we'd
    // charge in a different currency than was presented to the user.
    if ($payment_amount->getCurrencyCode() !== "EUR") {
      throw new DeclineException("Stripe Sofort requires payments to be in euros");
    }

    $order = $payment->getOrder();


    $sourceId = $order->getData('commerce_stripe_sofort_source');

    try {

      $source = $sourceId ? Source::retrieve($sourceId) : FALSE;

      $allowed_statuses = [
        Source::STATUS_CANCELED,
        Source::STATUS_CHARGEABLE,
        Source::STATUS_CONSUMED,
        Source::STATUS_FAILED,
        Source::STATUS_PENDING,
      ];

      if ($source instanceof Source && in_array($source->status, $allowed_statuses) && $source->type == 'sofort') {

        if ($source->redirect->url) {
          $url = $source->redirect->url;
        }
        //fallback?
      }
      elseif (!empty($returnUrl)) {
        $source = Source::create([
          "type" => "sofort",
          'amount' => $this->toMinorUnits($order->getTotalPrice()),
          "currency" => $order->getTotalPrice()->getCurrencyCode(),
          "redirect" => [
            'return_url' => $returnUrl,
          ],
          'metadata' => [
            'order_id' => $order->id(),
            'store_id' => $order->getStoreId(),
            'store_name' => $order->getStore()->label(),
            'email' => $order->getEmail(),
          ],
          "sofort" => [
            'country' => $order->getBillingProfile()
              ->get('address')
              ->first()
              ->getCountryCode(),
          ],
        ]);

        $order->setData('commerce_stripe_sofort_source', $source->id)->save();
        $order->setData('commerce_stripe_sofort_secret', $source->client_secret)
          ->save();
        if ($source->redirect->url) {
          $url = $source->redirect->url;
        }

        // Update the local payment entity.
        $payment->setState('authorization');
        $payment->setRemoteId($source->id);
        $payment->save();
      }
    } catch (ApiErrorException $e) {
      $this->logger->warning($e->getMessage());
    }

    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $source = $request->get('source');
    $source = Source::retrieve($source);
    $paymentClientSecret = $request->get('client_secret');
    $payment = NULL;
    $parameters = [];
    $payment = $this->findPayment($source, $paymentClientSecret);

    //payment with that payment stripe payment source and secret not found
    if (is_null($payment)) {
      throw new InvalidRequestException("Invalid payment specified.");
    }

    $order = $payment->getOrder();

    if ($order->isPaid()) {
      throw new InvalidRequestException('Order for this payment is already paid in full');
    }

    if ($payment->getState()->value !== 'completed') {
      /** @var \Drupal\commerce_stripe_sofort\Plugin\Commerce\PaymentGateway\StripeSofort $gateway */
      try {
        $source = Source::retrieve($source);
      } catch (ApiErrorException $e) {
        throw new InvalidRequestException('Unable to fetch Stripe payment source');
      }

      switch ($source->status) {
        case Source::STATUS_CONSUMED:
          $this->chargeConsumed($order, $source);
          $payment = $this->paymentStorage
            ->load($payment->id());
          break;

        case Source::STATUS_CHARGEABLE:
          $this->sourceSucceeded($source, $order);
          $this->chargeConsumed($order, $source);
          $this->createPaymentMethod($order, $payment->getPaymentGateway(), $source->id);
          $payment = $this->paymentStorage
            ->load($payment->id());

          break;

        case Source::STATUS_CANCELED:
        case Source::STATUS_FAILED:
          // Void transaction.
          $payment = $this->sourcePaymentFailed($source);
          $this->messenger()->addStatus('Payment failed.');
          break;
      }
    }

    // If the payment has been completed in webhooks there's nothing to do.
    switch ($payment->getState()->value) {
      case 'completed':
        $parameters = [
          'source' => $request->get('source'),
          'client_secret' => $request->get('client_secret'),
        ];
        $route = 'commerce_checkout.form';
        break;

      default:
        $route = 'commerce_checkout.form';
        break;
    }

    $url = Url::fromRoute($route, [
      'commerce_order' => $order->id(),
      'step' => 'checkout',
    ], [
      'query' => $parameters,
      'absolute' => TRUE,
    ])->toString();
    return RedirectResponse::create($url);
  }

  /**
   * Finds the payment by remote_id and client_secret combination.
   *
   * @param string $remote_id
   *   Remote id.
   * @param string|bool $client_secret
   *   If not empty checks if client secret matches the one on payment source.
   *
   * @return \Drupal\commerce_payment\Entity\Payment|null
   *   Either the found payment entity or null if nothing could be found.
   */
  public function findPayment($remote_id, $client_secret = FALSE) {
    /** @var \Drupal\commerce_payment\Entity\Payment[] $payments */
    $payments = $this->paymentStorage
      ->loadByProperties([
        'remote_id' => $remote_id,
      ]);

    if (empty($payments)) {
      return NULL;
    }
    $payment = reset($payments);
    if ($payment instanceof Payment && $client_secret) {
      $order = $payment->getOrder();
      if ($order instanceof Order && $client_secret != $order->getData('commerce_stripe_sofort_secret')) {
        $payment = NULL;
      }
    }

    return $payment;
  }

  /**
   * When charge consumed handle payment statuses and authorization
   *
   * @param Order $order
   *   Drupal commerce Order Entity
   *
   * @return \Drupal\commerce_payment\Entity\Payment|null
   *   Payment.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function chargeConsumed(Order $order) {
    $charge = Charge::retrieve($order->getData('commerce_stripe_sofort_charge_id'));

    if ($charge->paid == TRUE) {

      $payment = $this->findPayment($order->getData('commerce_stripe_sofort_source'), $order->getData('commerce_stripe_sofort_secret'));

      if ($payment instanceof Payment) {
        $workflow = $payment->getState()->getWorkflow();
        $transition = $workflow->getTransition('capture');
        $payment->getState()->applyTransition($transition);
        $request_time = $this->time->getRequestTime();
        $payment->setCompletedTime($request_time);
        $payment->save();
        $event = new CommerceStripeSofortEvent($payment);
        // $order->setData('commerce_stripe_sofort_source', NULL);
        // $order->setData('commerce_stripe_sofort_secret', NULL);
        $this->eventDispatcher->dispatch(CommerceStripeSofortEvents::COMMERCE_STRIPE_SOFORT_PAYMENT_SUCCEEDED, $event);
      }
      return $payment;
    }
  }

  /**
   * If Stripe source successfull create an charge
   *
   * @param \Stripe\Source $source
   *   The data Stripe provides for this event.
   *
   * @param Order $order
   *   Drupal commerce Order Entity
   *
   * @return \Drupal\commerce_payment\Entity\Payment|null
   *   Payment.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function sourceSucceeded(Source $source, Order $order) {
    return $this->createCharge($source, $order);
  }

  /**
   * After the source is chargeable, create a Stripe charge object
   *
   * @param \Stripe\Source $ource
   *   The data Stripe provides for this event.
   *
   * @param Order $order
   *   Drupal commerce Order Entity
   *
   * @return \Stripe\Charge $charge
   *   Payment.
   *
   */
  protected function createCharge(Source $source, Order $order) {
    $charge = Charge::create([
      'amount' => $source->amount,
      'currency' => $source->currency,
      'source' => $source->id,
      'metadata' => [
        'order' => $order->id,
      ],
    ]);
    $order->setData('commerce_stripe_sofort_charge_id', $charge->id)->save();
    return $charge;
  }

  /**
   * @{inheritdoc}
   */
  public function createPaymentMethod(OrderInterface $order, PaymentGatewayInterface $payment_gateway, string $paymentMethod) {
    $billing_profile = $order->getBillingProfile();
    if ($billing_profile instanceof ProfileInterface) {
      $type = 'commerce_stripe_sofort';

      /** @var \Drupal\commerce_payment\PaymentMethodStorageInterface $payment_method_storage */
      $payment_method_storage = Drupal::entityTypeManager()->getStorage('commerce_payment_method');
      $payment_method = $payment_method_storage->create([
        'type' => $type,
        'payment_gateway' => $payment_gateway->id(),
        'remote_id' => $paymentMethod,
        'uid' => $order->getCustomerId(),
        'billing_profile' => $order->getBillingProfile(),
      ]);
      $order->set('payment_method', $payment_method)->save();
      return $payment_method;
    }

    return [
      'type' => 'errors',
      'errors' => [
        [
          'status' => '422',
          'title' => 'Payment Method Error',
          'detail' => 'Billing profile does not exist on cart order!',
        ],
      ],
    ];
  }

  /**
   * if Stripe source payment failed
   *
   * @param \Stripe\Source $source
   *   The data Stripe provides for this event.
   *
   * @return \Drupal\commerce_payment\Entity\Payment|null
   *   Payment.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function sourcePaymentFailed(Source $source) {
    $payment = $this->findPayment($source->id, $source->client_secret);
    if ($payment instanceof Payment) {
      $workflow = $payment->getState()->getWorkflow();
      $transition = $workflow->getTransition('void');
      $payment->getState()->applyTransition($transition);
      $payment->save();
      $event = new CommerceStripeSofortEvent($payment);
      $this->eventDispatcher->dispatch(CommerceStripeSofortEvents::COMMERCE_STRIPE_SOFORT_PAYMENT_FAILED, $event);
    }
    return $payment;
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {

    $this->assertPaymentState($payment, ['authorization']);
    // Void Stripe payment - release uncaptured payment.
    try {
      $order = $payment->getOrder();
      $charge = Charge::retrieve($order->getData('commerce_stripe_sofort_charge_id'));

      if ($charge instanceof Charge) {
        $statuses_to_void = [
          Charge::STATUS_PENDING,
          Charge::STATUS_FAILED,
        ];
        if (!in_array($charge->status, $statuses_to_void)) {
          throw new PaymentGatewayException('The Charge cannot be voided because its not in allowed status.');
        }
      }
      else {
        $message = $this->t('Charge could not be retrieved from Stripe. Please check data on Stripe.');
        $this->logger->warning($message);
        throw new PaymentGatewayException($message);
      }
    } catch (ApiErrorException $e) {
      $this->logger->warning($e->getMessage());
      throw new PaymentGatewayException('Void failure. Please check Stripe Logs for more info.');
    }
    $payment->setState('authorization_voided');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    try {
      $order = $payment->getOrder();
      $charge = Charge::retrieve($order->getData('commerce_stripe_sofort_charge_id'));

      $refund = Refund::create([
        'charge' => $charge->id,
      ]);
      $order->setData('commerce_stripe_sofort_refund', $refund->id)->save();
    } catch (ApiErrorException $e) {
      $this->logger->warning($e->getMessage());
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }
    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    // Void Stripe payment - release uncaptured payment.
    try {
      $order = $payment->getOrder();
      $charge = Charge::retrieve($order->getData('commerce_stripe_sofort_charge_id'));

      if ($charge instanceof Charge) {
        $statuses_to_void = [
          Charge::STATUS_PENDING,
          Charge::STATUS_FAILED,
        ];
        if (!in_array($charge->status, $statuses_to_void)) {
          throw new PaymentGatewayException('The Charge cannot be voided because its not in allowed status.');
        }
      }
      else {
        $message = $this->t('Charge could not be retrieved from Stripe. Please check data on Stripe.');
        $this->logger->warning($message);
        throw new PaymentGatewayException($message);
      }
    } catch (ApiErrorException $e) {
      $this->logger->warning($e->getMessage());
      throw new PaymentGatewayException('Void failure. Please check Stripe Logs for more info.');
    }
    $payment->setState('authorization_voided');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayLabel() {
    if ($this->configuration['display_title_override']) {
      return Markup::create($this->configuration['display_title_override'])
        ->__toString();
    }
    if ($this->configuration['logo']) {
      return Markup::create('<img alt="sofort" src="https://cdn0.iconfinder.com/data/icons/payment-methods-19/24/Sofort_payments_pay_online_send_money_credit_card_ecommerce-512.png">')
        ->__toString();
    }

    return parent::getDisplayLabel();
  }

}
