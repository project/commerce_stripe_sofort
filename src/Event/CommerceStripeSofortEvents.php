<?php

namespace Drupal\commerce_stripe_sofort\Event;

/**
 * Defines events for the Commerce Stripe Sofort module.
 */
final class CommerceStripeSofortEvents {

  /**
   * Name of the event fired when payment succeeded.
   *
   * @Event
   *
   * @see \Drupal\commerce_stripe_sofort\Event\CommerceStripeSofortEvent
   */
  const COMMERCE_STRIPE_SOFORT_PAYMENT_SUCCEEDED = 'commerce_stripe_sofort.payment_succeeded';

  /**
   * Name of the event fired when payment failed.
   *
   * @Event
   *
   * @see \Drupal\commerce_stripe_sofort\Event\CommerceStripeSofortEvent
   */
  const COMMERCE_STRIPE_SOFORT_PAYMENT_FAILED = 'commerce_stripe_sofort.payment_failed';

}
