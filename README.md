# Stripe sofort
This is the module for extending the commerce_stripe plugin to accept sofort.
Sofort is an async payment available in Austria, Belgium, Germany, Italy, Netherlands and Spain. Approval can take from two days to 14 days (in averege)
Currently it is written with Stripe Source and Stripe Charge, but in the future Stripe is planing to redo this with payment intents.

## Requirements
Stripe PHP library ( included via composer )

## Endpoint
The endpoint for this module is /stripe-sofort-webhook (yourdomain/stripe-sofort-webhook). In the stripe webhook configuration you need to set that webhook, and mark all of the charge events to be logged.
Unlike Stripe's payment intent, charge does not have a return_url.

## Local enviroment testing
We are using endpoints so running on your local instance your site should be exposed over one of the following options:
* Ngrok (https://ngrok.com/)
* Expose (https://beyondco.de/docs/expose/introduction)
* In addition to that you need to add this
module https://www.drupal.org/project/ngrok_drupal to have sessions working (logging in, carts etc)

### Setting up in Stripe dashboard
Go to the stripe webhooks and add your exposed address.
Under event types select charge (13 events will be selected).
Copy the signature for your specific environment and place it in the Drupal payment configuration.

## Configuration

* Install and enable the plugin
* Machine name (Payment gateway id ) must be "commerce_sofort_stripe"
* Austria, Belgium, Germany, Italy, Netherlands and Spain
* add publishable key, secret key and signing secret.
* Setup the countries available to be: Austria, Belgium, Germany, Italy, Netherlands and Spain
